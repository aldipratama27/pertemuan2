/**
 * This view is an example list of people.
 */
Ext.define('Pertemuan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist', //xtype berguna untuk menampilkan apa yang di pilih dalam sub menu (home)

    requires: [
        'Pertemuan.store.Personnel'
    ],

    title: 'Data Anggota Sencha Kelompok study club',

    store: {
        //type: 'personnel'

    },

    columns: [
           { text: 'Paket',  dataIndex: 'paket', width: 200 },
           { text: 'Harga',  dataIndex: 'harga', width: 130 },
           { text: 'Diskon', dataIndex: 'diskon', width: 150 }
        // { text: 'NPM',        dataIndex: 'npm',          width: 200 },
        // { text: 'Nama',       dataIndex: 'name',         width: 230 },
        // { text: 'Email',      dataIndex: 'email',        width: 240 },
        // { text: 'Telepon',    dataIndex: 'phone',        width: 150 },
        // { text: 'Kota',       dataIndex: 'kota',         width: 150 },
        // { text: 'Organisasi', dataIndex: 'organisasi',   width: 150 }
    ],

    listeners: {
        select: 'onDataDipilih'
    }
});
