/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Pertemuan.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Pertemuan.view.main.MainController', //deklarasi terhadapat MainController.js, untuk terkoneksi maka di jadikan header
        'Pertemuan.view.main.MainModel',//deklarasi class MainModel.js
        'Pertemuan.view.form.User', //deklarasi dari class User.js
        'Pertemuan.view.group.Carousel',
        'Pertemuan.view.setting.BasicDataView',
        'Pertemuan.view.main.List', //deklarasi List.js di header untuk menghubungkan class utama program ke sub program(List.js merupakan home)
        'Pertemuan.view.form.Login',
        'Pertemuan.view.chart.Column',
        'Pertemuam.view.chart.ColumnPie',
        'Pertemuan.view.chart.ColumnLine',
        //'Pertemuan.view.tree.TreeList' //(7.2) deklarasikan class
        'Pertemuan.view.tree.TreePanel' //(7.6) deklarasikan class
        
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'mainlist' //didapat dari List.js yang berguna untuk menghubungkan ke List.js
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'user' //didapat dari User.js yang berguna untuk menghubungkan ke User.js
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'mycarousel' //didapat dari User.js yang berguna untuk menghubungkan ke Group.js
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'basicdataview' //didapat dari User.js yang berguna untuk menghubungkan ke Setting.js
            }]
        },{
            title: 'Charts1',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'column-chart' //didapat dari User.js yang berguna untuk menghubungkan ke 
            }]
        },{
            title: 'Charts2',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'chart' //didapat dari User.js yang berguna untuk menghubungkan ke 
            }]
        },{
            title: 'Charts3',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'renderer' //didapat dari User.js yang berguna untuk menghubungkan ke BasicDataView.js
            }]
        },{
            title: 'Tree',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                //xtype: 'tree-list' //(7.3)didapat dari TreeList.js yang berguna untuk menghubungkan ke TreeList.js
                xtype: 'tree-panel'
            }]
        }
    ]
});
