Ext.define('Pertemuan.view.tree.TreeList', {
    extend: 'Ext.grid.Tree',
    xtype: 'tree-list',
    //(7.1)untuk penambahan tree seperti biasa copypaste change class and add xtype
    requires: [
        'Ext.grid.plugin.MultiSelection'
    ],

    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{navItems}',

    //(7.4) penambahan aksi yg akan di dengar
    listeners:{
        itemtap: function ( me, index, target, record, e, eOpts ){
            //detailtree = Ext.getCmp('detailtree'); //(7.12)
            //detailtree.setHtml("ur option" +record.data.text) //(7.13)
            //alert("ur option " +record.data.text);
            detailtree = Ext.getStore('personnel');
            detailtree.filter('organisasi',record.data.text);
        }
    }
});