Ext.define('Pertemuan.view.tree.TreePanel', {
    extend: 'Ext.Container',
    xtype: 'tree-panel',
    //(7.5) membuat panel serta copypaste sintak dari commponent-panels
    requires: [
        'Ext.layout.HBox',
        'Pertemuan.view.tree.TreeList' //(7.7) deklarasikan class TreeList.js berguna untuk deklarasi data yg akan di show
    ],
    layout: {
        type: 'hbox',
        pack: 'center',
        align: 'stretch'
    },
    margin: '0 10',
    defaults: {
        margin: '0 0 10 0',
        bodyPadding: 10
    },
    //(7.8) add items
    items: [
        {
            //(7.9) penampilakan data yg di treelist.js
            xtype: 'tree-list',
            flex: 1
        },
        {
            //(7.10) 
            //xtype: 'panel',
            //(7.11) panggilan data yang akan di tampilkan di TreeList.js, deklarasikan Id ini ya 
            //id: 'detailtree',
            //flex: 2,
            //html: 'belum ada terpilih'
            xtype:'panel',
            id:'detailtree',
            flex:1,
            xtype:'basicdataview'
        }
    ]
})
