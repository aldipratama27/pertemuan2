Ext.define('Pertemuan.view.chart.ColumnController3', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.renderer',

    onRefresh: function() {
        var chart = this.lookupReference('chart');

        chart.getStore().generateData(10);
    }

});