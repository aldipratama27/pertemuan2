/**
 * Demonstrates how to use an Ext.Carousel in vertical and horizontal configurations
 */
Ext.define('Pertemuan.view.group.Carousel', {
    extend: 'Ext.Container',
    xtype: 'mycarousel',

    requires: [
        'Ext.carousel.Carousel'
    ],

    shadow: true,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    /*defaults: {
        flex: 1
    },*/

    items: [ {
        xtype: 'carousel',
        //flex: 3,
        width: 100,
        id: 'carouselatas',
        items: [{
            html: '<p>Swipe left to show the next card…</p>'
        },
        {
            html: '<p>You can also tap on either side of the indicators.</p>'
        },
        {
            
            text: 'Back To Home'
                        
        }]
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'carousel',
        //flex: 1,
        width: 200,
        direction: 'vertical',
        items: [{
            html: '<p>Carousels can also be vertical <em>(swipe up)…</p>'
        },
        {
            html: 'And can also use <code>ui:light</code>.'
        },
        {
            html: 'Card #3'
        }]
    }]
});