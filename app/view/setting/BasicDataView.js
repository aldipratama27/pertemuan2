Ext.define('Pertemuan.view.setting.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'basicdataview',
    requires: [
        
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Pertemuan.store.Personnel',//requires dari List.js
        'Ext.field.Search' //(6.1) requires dari toolbars-toolbar input search
    ],

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{ //(6.2) penambahanan field search
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'searchfield',
                    placeHolder: 'Search by name',
                    name: 'searchfield',
                    //(6.3)penambahan apa yg akan di dengar oleh search
                    //(6.4)go to store Personel.js
                    listeners: {
                        change: function(me, newValue, oldValue, eOpts){
                            //alert(newValue);
                            //(6.7)berfungsi untuk menampilkan interaksi search ke store
                            personnelStore = Ext.getStore('personnel');
                            //(6.6) modern toolkit, cari store-methods-filter (6.8) serta deklarasikan variabel yg ingin di filter
                            personnelStore.filter('name', newValue);
                        }
                    }
                },
                {
                    extype: 'spacer'
                },
                {
                    xtype: 'searchfield',
                    placeHolder: 'Search by Country',
                    name: 'searchfield',
                    //(6.3)penambahan apa yg akan di dengar oleh search
                    //(6.4)go to store Personel.js
                    listeners: {
                        change: function(me, newValue, oldValue, eOpts){
                            //alert(newValue);
                            //(6.7)berfungsi untuk menampilkan interaksi search ke store
                            personnelStore = Ext.getStore('personnel');
                            //(6.6) modern toolkit, cari store-methods-filter (6.8) serta deklarasikan variabel yg ingin di filter
                            personnelStore.filter('kota', newValue);
                        }
                    }
                },
                {
                    extype: 'spacer'
                },
                {
                    xtype: 'searchfield',
                    placeHolder: 'Search by NPM',
                    name: 'searchfield',
                    //(6.3)penambahan apa yg akan di dengar oleh search
                    //(6.4)go to store Personel.js
                    listeners: {
                        change: function(me, newValue, oldValue, eOpts){
                            //alert(newValue);
                            //(6.7)berfungsi untuk menampilkan interaksi search ke store
                            personnelStoree = Ext.getStore('personnel');
                            //(6.6) modern toolkit, cari store-methods-filter (6.8) serta deklarasikan variabel yg ingin di filter
                            personnelStoree.filter('npm', newValue);
                        }
                    }
                }

            ]
        },{
            xtype: 'dataview',
            scrollable: 'y',
            cls: 'dataview-basic',
            itemTpl: '{photo}<font size=4 color="red">-{npm}</font><br><br>-<b>{name}</b></br>-<i>{email}</i><br>-<u>{phone}</u><br>-{kota}</br><br><b>-{organisasi}</b></br></br><hr>',// tugas 1 deskripsinya itu menggunakan data kurung kurawal pada line ini, data ini dapat dilihat dari model (model kita ada store tipenya adalah personnel. yg datanya ada di fields:)
            /*store: {
                type: 'personnel'//dari List.js
            }, ///'personnel',//dari List.js*/
            bind: {
                store: '{personnel}', //kurung kurawal pd personnel berfungsi mengikat database di personnel type, viewModel
            },
            plugins: {
                type: 'dataviewtip', //berguna ketika kusor di arahkan ke foto maka data2nya akan tampil
                align: 'l-r?',
                plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            //delegate: '.photo', //berguna untuk pop ap deskripsi data pada gambar //tugas 2 memberi gambar pd data view
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' +
                    '<tr><td></td><td>{photo}</td></tr>'+ 
                    '<tr><td>NPM        :</td><td>{npm}  </td></tr>' +
                    '<tr><td>Name       :</td><td>{name} </td></tr>' + 
                    '<tr><td>email      :</td><td>{email}</td></tr>' + 
                    '<tr><td>phone      :</td><td>{phone}</td></tr>' +
                    '<tr><td>Kota       :</td><td>{kota}</td></tr>' +
                    '<tr><td>Organisasi :</td><td>{organisasi}</td></tr>' +
                    '<tr><td vAlign="top">Bio:</td><td><div style="max-height:100px;overflow:auto;padding:1px">{bio}</div></td></tr>'
        }
    }]
});