Ext.define('Pertemuan.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel', //(6.5)menambahkan storeId

    alias: 'store.personnel',
    autoLoad: true,
    // fields: [
    //     'npm', 'name', 'email', 'phone', 'photo', 'kota', 'organisasi'
    // ],

    // data: { items: [
    //     { photo:'<img src="resources/pic2.jpg" alt="" width="100" height="80"><br>', npm: '183510672', name: 'Aldi pratama',     email: "Aldipratama27@student.uir.ac.id", phone: "0882-7106-1518", kota: "Pekanbaru", organisasi: 'himatif' },
    //     { photo:'<img src="resources/pic1.jpg" alt="" width="100" height="80"><br>', npm: '183510331', name: 'Doni saputra',     email: "Doni.saputra@enterprise.com",     phone: "0812-0909-0002", kota: "Jakarta",   organisasi: 'himatif' },
    //     { photo:'<img src="resources/pic3.jpg" alt="" width="100" height="80"><br>', npm: '183510121', name: 'Rini febriana',    email: "Rini.febriana@enterprise.com",    phone: "0898-2122-0031", kota: "Surabaya",  organisasi: 'groot'   },
    //     { photo:'<img src="resources/pic4.jpg" alt="" width="100" height="80"><br>', npm: '183510126', name: 'Della angriani',   email: "Della.angriani@enterprise.com",   phone: "0852-7965-3883", kota: "Pekanbaru", organisasi: 'groot'   }
    // ]},   

    fields: [
        'id_harga', 'paket', 'harga', 'diskon'
    ],

    

    proxy: {
        type: 'jsonp',
        api: {
            read   : "http://localhost/php//readpricelist.php",
            //read   : Pertemuan.util.Globals.getPhppath()+'/php/readpricelist.php',
            //update : Pertemuan.util.Globals.getPhppath()+'/php/updatepricelist.php',
            //insert : Pertemuan.util.Globals.getPhppath()+'/php/insertpricelist.php',
            //remove : Pertemuan.util.Globals.getPhppath()+'/php/removepricelist.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
